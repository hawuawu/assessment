package com.hawuawu.assessment.model

case class Group(groupdId: String, addressId: String, numberOfCustomers: Int, fromDate: Int, toDate: Int)
