package com.hawuawu.assessment.model

case class AddressData( customerId: String, addressId: String, fromDate: Int, toDate: Int)